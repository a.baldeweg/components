# Changelog

## 2.0.0

- build: remove npm-run-all
- build: update dependencies
- fix: new forms
- fix: do not show empty dropdown
- feature: add bottom positioning to dropdown
- fix: input
- fix: select
- fix: textarea
- feature: autosuggest
- feature: forms
- feature: click-title event
- fix: dropdown
- fix: type in b-button renamed to design
- feature: sleeptimer icon
- fix: hide dropdown after click in dropdown
- fix: write no-hover instead of noHover
- fix: colors of dropdown
- feature: add border radius to dropdown
- fix: remove margins from b-dropdown-divider
- refactor: b-dropdown-item
- feature: validation service
- refactor: move notification
- refactor: icons
- refactor: b-dropdown
- refactor: dropdown is not part of list anymore
- fix: align left
- build: show progress in webpack while building

## 1.4.0

- fix: notification bar path
- build: update dependencies
- doc: changelog
- feature: show profile icon
- fix: no hovering
- feature: no hovering effect in icons
- feature: mixin
- fix: remove padding
- feature: mixin for icons
- feature: modal
- fix: break words
- feature: dropdown divider
- fix: closing dropdown
- fix: show mouse pointer
- feature: export notification as instance method
- feature: dropdown icons
- test: fix test
- fix: remove b-notification-bar
- refactor: rename caution to warning
- feature: notifications

## 1.3.0

- bump version
- build: update dependencies
- fix: show current theme
- doc: example moved to notification
- doc: move directives above components
- feature: toggle
- fix: missing type
- feature: align content in containers
- feature: flexible width for dropdown
- feature: focus directive
- feature: dark mode component
- feature: icons are sizable
- feature: fit logo
- fix: typo in BListSeparator
- feature: export seperator

## 1.2.0

- build
- fix: trailing comma
- build: bump version
- fix: rename context menu
- fix: linting
- build: update dependencies
- fix: remove padding on the right
- fix: align items center
- feature: add form example
- fix: return value
- docu: class mentioned
- docu: remove three-col
- fix: add docu for notification-bar
- feature: more icon

## 1.1.0

- build
- build: bump version
- fix: offcanvas open and close
- feature: make the title optional
- feature: submit event
- feature: option for menu and settings
- build

## 1.0.0

- First  Release
